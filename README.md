![c,s,v](csv.gif)

# Bitbucket CSV Table

The Bitbucket CSV Table displays CSV files as a table that is easier to read. It is an entirely client-side Bitbucket addon
using a [file viewer](https://legacy-developer.atlassian.com/bitbucket/modules/fileViews.html) module.

This was done for [ShipIt 39](https://shipit.atlassian.net/browse/SHPXXXIX-346)

[See the CSV Table in action.](csv-table.gif)

## Installation

Find the addon on the [Bitbucket integrations](https://bitbucket.org/account/addon-directory/) page
or go to it directly with [this link](https://bitbucket.org/account/addon-directory?addon=csv-table) once it gets published.

Before it’s published you can install it manually with the URL to the [descriptor.json](https://bbcsv.us-west-1.prod.public.atl-paas.net/descriptor.json)
on the [manage integrations](https://bitbucket.org/account/addon-management) page.

## Sample Files

* [titans.csv](./samples/titans.csv)
* [empty.csv](./samples/empty.csv)
* [html-tags.csv](./samples/html-tags.csv)
* [trailing-newline.csv](./samples/trailing-newline.csv)
* [blank-lines.csv](./samples/blank-lines.csv)

## Development
```
yarn
yarn prod:build
```

## Deployment

This app is fully static and hosted on S3, deployed via micros.

* Service: https://services.atlassian.io/services/bbcsv
* CDN Address: https://bbcsv.us-west-1.prod.public.atl-paas.net
* S3 Address:  http://bbcsv.us-west-1.prod.public.atl-paas.net.s3-website-us-west-1.amazonaws.com

## Kudos
* Kudos to [Papa Parse](http://papaparse.com/) for the CSV parser.
* Kudos to @evzijst for the new [v2 file API](https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/src/%7Bnode%7D/%7Bpath%7D).
* Kudos to @csomme for figuring out how to request non-JSON data with connect.

## TODO

* ADG tables
* linkify URLs