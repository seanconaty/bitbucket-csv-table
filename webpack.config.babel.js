import path from 'path'

const HtmlWebpackPlugin = require('html-webpack-plugin');

export default {
  entry: [
    './src',
  ],
  output: {
    filename: 'js/[hash].bundle.js',
    path: path.resolve(__dirname, 'site/dist'),
    publicPath: '/dist/',
  },
  module: {
    rules: [
      { test: /\.js$/, use: 'babel-loader', exclude: /node_modules/ },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/iframe.ejs'),
    }),
  ],
  devtool: false,
  resolve: {
    extensions: ['.js',],
  },
  devServer: {
    port: 7000,
  },
}
