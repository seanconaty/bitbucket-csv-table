import 'babel-polyfill';
import Papa from 'papaparse';
import queryString from 'query-string';
import escapeHtml from 'escape-html';

const params = queryString.parse(document.location.search);
let data;
let columnCount;


AP.require('request', function(request){
  request({
    url: params.v2FileUrl,
    responseType: 'text/plain',
    success: function(response) {
      handleSuccess(response);
    },
    error: function() {
      displayError("Bad request.");
    },
  });
});

const handleSuccess = function(response) {
  const parsed = Papa.parse(response, {
    skipEmptyLines: true,
  });
  columnCount = parsed.data[0] && parsed.data[0].length;

  if (!columnCount) {
    displayError("This CSV file is empty.");
    return;
  }
  data = parsed.data;

  // handle trailing newlines in files
  const lastId= data.length - 1;
  if (data[lastId].length === 1 && data[lastId][0] === '') {
    delete data[lastId];
  }

  renderTable();
}

const renderTable = function() {
  const html = data.reduce((rows, row, i) => `${rows}<tr><td class='lineno'>${i+1}</td>${
    row.reduce((cells, cell) => `${cells}<td>${escapeHtml(cell)}</td>`, "")
  }</tr>`, "");

  document.querySelector('#root').innerHTML = `<table class="aui"><tbody>${html}</tbody></table>`;
}

const displayError = function(message) {
  document.querySelector('#root').innerHTML = message;
}

